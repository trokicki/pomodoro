﻿namespace Pomodoro {
	partial class Pomodoro {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pomodoro));
			this.BtnStart = new System.Windows.Forms.Button();
			this.LblStatus = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.LblTimeToChange = new System.Windows.Forms.Label();
			this.LblChangeTime = new System.Windows.Forms.Label();
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.SuspendLayout();
			// 
			// BtnStart
			// 
			this.BtnStart.BackColor = System.Drawing.Color.Green;
			this.BtnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.BtnStart.Location = new System.Drawing.Point(3, 143);
			this.BtnStart.Name = "BtnStart";
			this.BtnStart.Size = new System.Drawing.Size(247, 66);
			this.BtnStart.TabIndex = 0;
			this.BtnStart.Text = "START";
			this.BtnStart.UseVisualStyleBackColor = false;
			this.BtnStart.Click += new System.EventHandler(this.button1_Click);
			// 
			// LblStatus
			// 
			this.LblStatus.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.LblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.LblStatus.Location = new System.Drawing.Point(3, 2);
			this.LblStatus.Name = "LblStatus";
			this.LblStatus.Size = new System.Drawing.Size(241, 22);
			this.LblStatus.TabIndex = 2;
			this.LblStatus.Text = "[click START button]";
			this.LblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label1.Location = new System.Drawing.Point(4, 52);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(104, 16);
			this.label1.TabIndex = 3;
			this.label1.Text = "Time to change:";
			// 
			// LblTimeToChange
			// 
			this.LblTimeToChange.AutoSize = true;
			this.LblTimeToChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.LblTimeToChange.Location = new System.Drawing.Point(106, 51);
			this.LblTimeToChange.Name = "LblTimeToChange";
			this.LblTimeToChange.Size = new System.Drawing.Size(0, 20);
			this.LblTimeToChange.TabIndex = 4;
			// 
			// LblChangeTime
			// 
			this.LblChangeTime.AutoSize = true;
			this.LblChangeTime.Location = new System.Drawing.Point(7, 88);
			this.LblChangeTime.Name = "LblChangeTime";
			this.LblChangeTime.Size = new System.Drawing.Size(69, 13);
			this.LblChangeTime.TabIndex = 6;
			this.LblChangeTime.Text = "Change time:";
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.notifyIcon1.BalloonTipText = "Change";
			this.notifyIcon1.BalloonTipTitle = "Pomodoro";
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "NotifyIcon";
			this.notifyIcon1.Visible = true;
			this.notifyIcon1.BalloonTipClicked += new System.EventHandler(this.notifyIcon1_BalloonTipClicked);
			this.notifyIcon1.Click += new System.EventHandler(this.notifyIcon1_Click);
			this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
			// 
			// Pomodoro
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Tomato;
			this.ClientSize = new System.Drawing.Size(254, 216);
			this.Controls.Add(this.LblChangeTime);
			this.Controls.Add(this.LblTimeToChange);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.LblStatus);
			this.Controls.Add(this.BtnStart);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Location = new System.Drawing.Point(1650, 900);
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(270, 254);
			this.MinimumSize = new System.Drawing.Size(270, 254);
			this.Name = "Pomodoro";
			this.Opacity = 0.9D;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Pomodoro";
			this.Resize += new System.EventHandler(this.Pomodoro_Resize);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button BtnStart;
		private System.Windows.Forms.Label LblStatus;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label LblTimeToChange;
		private System.Windows.Forms.Label LblChangeTime;
		private System.Windows.Forms.NotifyIcon notifyIcon1;
	}
}

