﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pomodoro {
	public partial class Pomodoro : Form {
		private enum WorkingStatus { WORKING, BREAK };
		private Timer timer = new Timer();
		private DateTime changeDate;
		private WorkingStatus workingStatus = WorkingStatus.BREAK;
		private Dictionary<string, string> strings = new Dictionary<string, string> {
			{"ChangeTime", "Change time: "},
			{"Working", "WORKING"},
			{"Break", "BREAK"}
		};
		private TimeSpan workingTimespan = new TimeSpan(0, 25, 0);
		private TimeSpan breakTimespan = new TimeSpan(0, 5, 0);

		public Pomodoro() {
			InitializeComponent();
		}

		void timer_Tick(object sender, EventArgs e) {
			TimeSpan dateDiff = changeDate.Subtract(DateTime.Now);
			if (dateDiff.TotalSeconds <= 0) {
				switchStatus();
			}
			LblTimeToChange.Text = dateDiff.ToString(@"mm\:ss");
		}

		private void button1_Click(object sender, EventArgs e) {
			initTimer();
			startWorking();
		}

		private void switchStatus() {
			if (workingStatus == WorkingStatus.WORKING) {
				startBreak();
				notifyIcon1.ShowBalloonTip(1000, "Break time!", "You've got 5 minutes. Go!", ToolTipIcon.Info);
			} else {
				startWorking();
				notifyIcon1.ShowBalloonTip(1000, "Get back to work...", "Just 25 minutes. Gonna be awesome for sure.", ToolTipIcon.Info);
			}
		}
		
		private void startBreak() {
			workingStatus = WorkingStatus.BREAK;
			changeDate = DateTime.Now.Add(breakTimespan);
			LblStatus.Text = strings["Break"];
			LblChangeTime.Text = String.Format("{0}{1}", strings["ChangeTime"], changeDate.ToString());
		}

		private void startWorking() {
			workingStatus = WorkingStatus.WORKING;
			changeDate = DateTime.Now.Add(workingTimespan);
			LblStatus.Text = strings["Working"];
			LblChangeTime.Text = String.Format("{0}{1}", strings["ChangeTime"], changeDate.ToString());
		}

		private void initTimer() {
			timer.Interval = 200;
			timer.Start();
			timer.Tick += timer_Tick;
		}

		private void Pomodoro_Resize(object sender, EventArgs e) {
			if (WindowState == FormWindowState.Minimized)
				this.Hide();
		}

		private void notifyIcon1_Click(object sender, EventArgs e) {
			TimeSpan datediff = changeDate.Subtract(DateTime.Now);
			if (datediff.TotalSeconds > 0)
				notifyIcon1.ShowBalloonTip(100, "Time to change", String.Format("Time left: {0}", datediff.ToString(@"mm\:ss")), ToolTipIcon.Info);
		}

		private void notifyIcon1_DoubleClick(object sender, EventArgs e) {
			this.Show();
			WindowState = FormWindowState.Normal;
		}

		private void notifyIcon1_BalloonTipClicked(object sender, EventArgs e) {
			this.Show();
			WindowState = FormWindowState.Normal;
		}
	}
}
